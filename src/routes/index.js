function setRoute(app) {
    app.use("/azure", require("./analysis/analysis-controller"));
};

module.exports = {
    setRoute
};