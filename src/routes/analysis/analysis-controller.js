const router = require('express').Router();
const oledb = require('oledb');
const analysis = require('../../config/config')

router.get("/", getData);

//get data from cube
async function getData(req, res, next) {

  var connectionString = `Provider=${analysis.provider};Data Source=${analysis.data_source};Initial catalog=${analysis.catalog};User ID=app:${analysis.app_id};Password=${analysis.auth_key};`;
  const db = oledb.oledbConnection(connectionString);

  // db.query(`EVALUATE (
  //                   TOPN (
  //                     10,
  //                     SUMMARIZECOLUMNS (
  //                       'finance ACCOUNT_H1'[ZNODE],
  //                       'finance ACCOUNT_H1'[ZPARENT]
  //                     ),
  //                     'finance ACCOUNT_H1'[ZPARENT],1
  //                   )
  //                 )`
  db.query(`EVALUATE('finance ACCOUNT_STAT')`
    // db.query(`DEFINE MEASURE 'FINANCE FACT'[TEMP] = SUM('finance Fact'[YTD])
    //             EVALUATE
    //               SUMMARIZECOLUMNS
    //                 (
    //                     'finance Fact'[YEAR],
    //                      TREATAS({2019,2020},'finance Fact'[YEAR]),
    //                     "TOTAL VALUE",CALCULATE(SUM('finance Fact'[SignedDataNew]))
    //                 )`
  ).then(result => {
    console.log(result);
    res.send(result);
  },
    err => {
      console.error(err);
      res.send(err)
    });

}

module.exports = router;